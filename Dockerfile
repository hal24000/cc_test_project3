FROM python:3.6.6
MAINTAINER HAL24K "docker@hal24k.com"
ENV PYTHONUNBUFFERED=0


RUN apt-get update

# Copy install dependencies

RUN pip3 install --upgrade pip

RUN pip3 install numpy pylint pigar configobj>=5.0.6
RUN apt-get install -y scons

# Create work dir, and copy all files
WORKDIR /src

# Copy requirements.txt and install dependencies top level
COPY requirements.txt /src


RUN pip3 install \
        --no-cache-dir \
        -r /src/requirements.txt

# Copy app files to image
COPY ./src /src

ENV PYTHONPATH=/src:/src/cc_test_project3
ENV SERVER_HOST='0.0.0.0'
ENV SERVER_PORT=80
EXPOSE 80

CMD ["python3", "/src/runserver.py"]
